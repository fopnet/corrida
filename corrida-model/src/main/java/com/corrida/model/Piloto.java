package com.corrida.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.LinkedList;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Entidade que modela o Piloto do kart 
 * 
 * @author Felipe
 *
 */
public class Piloto {

	private String codigo = null;
	private String nome = null;
	private Integer posicaoChegada = null;
	private Duration tempoDecorrido = Duration.ZERO;
	private Volta melhorVolta = null;
	private LinkedList<Volta> voltas = null;

	public Piloto(String nomeOuRegistro) {
		if (nomeOuRegistro != null && nomeOuRegistro.matches("^\\d{1,3}\\s–\\s[\\w\\.]+$")) {
			int slashIdx = nomeOuRegistro.indexOf("–");
			this.setCodigo(nomeOuRegistro.substring(0, slashIdx - 1));
			this.setNome(nomeOuRegistro.substring(slashIdx + 1));
		} else {
			setNome(nomeOuRegistro);
		}
		this.voltas = new LinkedList<Volta>();
	}

	/********************************************************************************
	* Getters and Setters
	********************************************************************************/
	
	public Duration getTempoDecorrido() {
		return tempoDecorrido;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo.trim();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome.trim();
	}

	public Integer getPosicaoChegada() {
		return posicaoChegada;
	}

	public void setPosicaoChegada(Integer posicaoChegada) {
		this.posicaoChegada = posicaoChegada;
	}

	/********************************************************************************
	* Sobreescrita do Equals e HashCode e toString
	********************************************************************************/
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(codigo).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Piloto == false) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		final Piloto otherObject = (Piloto) obj;

		return new EqualsBuilder().append(this.codigo, otherObject.codigo).isEquals();
	}
	
	public String toString() {
		return this.codigo + " - " + this.nome; 
	}

	/********************************************************************************
	* Funcionalidades 
	********************************************************************************/
	
	/**
	 * Avalia se a volta adicionada é a mais rápida
	 * 
	 * @param v
	 * @return
	 */
	private boolean setMelhorVolta(Volta v) {
		if (this.melhorVolta == null) {
			this.melhorVolta = v;
			return true;
		} else if (this.melhorVolta.getTempo().compareTo(v.getTempo()) > 0) {
			this.melhorVolta = v;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Retorna o tempo da melhor volta
	 * @return
	 */
	public Duration getTempoMelhorVolta() {
		return Duration.between(LocalTime.MIN, this.melhorVolta.getTempo());
	}

	/**
	 * Calcula a velocidade média 
	 * @return
	 */
	public BigDecimal calcularVelocidadeMedia() {
		BigDecimal media = BigDecimal.ZERO;
		for (Volta volta : voltas) {
			media = media.add(volta.getVelocidadeMedia());
		}
		return media.divide(BigDecimal.valueOf(voltas.size()), 3, RoundingMode.DOWN);
	}

	/**
	 * Adiciona a Volta na lista de voltas do piloto
	 * @param volta
	 */
	public void addVolta(Volta volta) {
		this.voltas.add(volta);

		// certificar que está ordenado pela hora da volta
		this.voltas.stream().max(Comparator.comparing(Volta::getHora)).get();
		
		// somo o tempo decorrido do piloto
		this.calcularTempoDecorrido(volta);

		// seto a melhor volta
		this.setMelhorVolta(volta);
	}

	/**
	 * Retorna o numero de voltas completas
	 * 
	 * @return
	 */
	public Integer getNumeroVoltasCompletas() {
		return this.voltas.size();
	}
	
	/**
	 * Calcula e mantém o tempo decorrido do piloto na corrida 
	 * 
	 * @param v
	 */
	private void calcularTempoDecorrido(Volta v) {
		this.tempoDecorrido = this.tempoDecorrido.plus(Duration.between(LocalTime.MIN, v.getTempo()));
	}

	/**
	 * Calcula o tempo decorrido em relacao ao vencedor
	 * @param vencedor
	 */
	public Duration calcularTempoDecorridoDoVencedor(Piloto vencedor) {
		return getTempoDecorrido().minus(vencedor.getTempoDecorrido());
	}

	

	
}
