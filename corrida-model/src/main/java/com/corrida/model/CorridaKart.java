package com.corrida.model;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Entidade que modela a corrida de kart.
 * 
 * @author Felipe
 *
 */
public class CorridaKart implements Corrida<Piloto> {

	private List<Volta> voltas = null;
	private Set<Piloto> pilotos = null;
	private Volta melhorVolta = null;
	private int numeroVoltasCorrida = 4;

	public CorridaKart() {
		this.voltas = new LinkedList<Volta>();
		this.pilotos = new HashSet<Piloto>();
	}

	public CorridaKart(Collection<Volta> lista) {
		this();
		lista.stream().forEach(v -> this.add(v));
	}


	/********************************************************************************
	* Getters and Setters
	********************************************************************************/

	public void setNumeroVoltasCorrida(int numeroVoltasCorrida) {
		this.numeroVoltasCorrida = numeroVoltasCorrida;
	}
	
	public Duration getTempoMelhorVolta() {
		return Duration.between(LocalTime.MIN, this.melhorVolta.getTempo());
	}

	/**
	 * Atribui a menor volta da corrida
	 * 
	 * @param v
	 */
	private void setMelhorVolta(Volta v) {
		if (this.melhorVolta == null || this.melhorVolta.getTempo().compareTo(v.getTempo()) > 0) {
			this.melhorVolta = v;
		}
	}

	
	/********************************************************************************
	* Funcionalidades
	********************************************************************************/
	
	/**
	 * 1. Adiciona a volta na lista de voltas do fornecedor 
	 * 2. Mantém a mesma instância do piloto nas instancias do tipo  'Volta'
	 * 
	 * @param v
	 * @return
	 */
	public boolean add(Volta v) {
		boolean result = voltas.add(v);
		if (!this.pilotos.add(v.getPiloto())) {
			Piloto mesmoPiloto = this.pilotos.stream().filter(p -> p.equals(v.getPiloto())).findFirst().get();

			v.setPiloto(mesmoPiloto);
		}

		final Piloto piloto = v.getPiloto();
		
		piloto.addVolta(v);
		
		setMelhorVolta(v);

		return result;
	}
	
	/**
	 * Calcular o ranking dos pilotos e o tempos dos pilotos em relação ao vencedor
	 * 
	 * A lista é ordenada em relação a posicao de chegada 
	 * 
	 * @return
	 */
	public List<Piloto> calcularTempoDoVencedorEPosicaoChegada() {
		List<Piloto> pilotoList = this.pilotos.stream()
				.sorted(
						Comparator.comparing(Piloto::getNumeroVoltasCompletas).reversed()
						.thenComparing(Piloto::getTempoDecorrido)
						)
				.collect(Collectors.toList());

//		for (int i = 0; i < pilotoList.size(); i++) {
//			pilotoList.get(i).setPosicaoChegada(i + 1);
//		}

		Piloto vencedor = getVencedor();

		return IntStream.range(0, pilotoList.size()).mapToObj(idx -> {

			Piloto p = pilotoList.get(idx);

			p.setPosicaoChegada(idx + 1);

			p.calcularTempoDecorridoDoVencedor(vencedor);

			return p;

		}).collect(Collectors.toList());
	}

	/**
	 * Retorna o piloto vencedor da corrida que completou 4 voltas
	 * 
	 * @return
	 */
	public Piloto getVencedor() {
		return this.voltas.stream()
				.filter(v -> v.getPiloto().getNumeroVoltasCompletas() == this.numeroVoltasCorrida)
				.min(Comparator.comparing(Volta::getHora))
				.get()
				.getPiloto();
	}
	

}
