package com.corrida.model;

import java.time.Duration;
import java.util.List;

public interface Corrida<P> {
	
	List<P> calcularTempoDoVencedorEPosicaoChegada();
	
	P getVencedor();

	Duration getTempoMelhorVolta();
	
}