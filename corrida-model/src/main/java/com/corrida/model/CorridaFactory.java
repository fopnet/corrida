package com.corrida.model;

import java.io.InputStream;
import java.util.List;

public class CorridaFactory  {

	static Corrida<Piloto> getCorridaKart(InputStream is) {
		List<Volta> voltas = VoltaUtil.load(is);
		return new CorridaKart(voltas);
	}

}
