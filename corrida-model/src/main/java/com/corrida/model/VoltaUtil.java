package com.corrida.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VoltaUtil {
	
	public static List<Volta> load(String filename) {
		
		List<Volta> list = new ArrayList<Volta>();
		
		try (Stream<String> stream = Files.lines(Paths.get(filename))) {
			
			list = streamToList(stream);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return list;
		
	}

	public static List<Volta> load(InputStream is) {
		
		List<Volta> list = new ArrayList<Volta>();
		
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(is))) {
			
			list = streamToList(buffer.lines());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	private static List<Volta> streamToList(Stream<String> stream) {
		return stream.skip(1).map(Volta.fromString).collect(Collectors.toList());
	}
	

}
