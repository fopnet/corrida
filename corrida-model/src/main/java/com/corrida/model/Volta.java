package com.corrida.model;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

/**
 * Entidade que modela uma Volta na corrida
 * @author Felipe
 *
 */
public class Volta {

	private Integer id = null;
	private LocalTime tempo = null;
	private BigDecimal velocidadeMedia = BigDecimal.ZERO;
	private LocalTime hora = null;

	private Piloto piloto = null;

	/********************************************************************************
	* Getters and Setters
	********************************************************************************/
	
	public Piloto getPiloto() {
		return piloto;
	}

	public void setPiloto(Piloto piloto) {
		this.piloto = piloto;
	}

	public void setPiloto(String nome) {
		this.piloto = new Piloto(nome);
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalTime getTempo() {
		return tempo;
	}

	public void setTempo(LocalTime tempo) {
		this.tempo = tempo;
	}

	public BigDecimal getVelocidadeMedia() {
		return velocidadeMedia;
	}
	
	public void setVelocidadeMedia(Double velocidadeMedia) {
		this.setVelocidadeMedia(BigDecimal.valueOf(velocidadeMedia));
	}

	public void setVelocidadeMedia(BigDecimal velocidadeMedia) {
		this.velocidadeMedia = velocidadeMedia;
	}


	/********************************************************************************
	* Função estática para o parser do leitor do arquivo
	********************************************************************************/
	
	static public Function<String, Volta> fromString = (line) -> {
		  String[] data = line.split("(\\s{2,})");// a CSV has comma separated lines
		  Volta v = new Volta();

		  v.setHora(LocalTime.parse(data[0]));
		  v.setPiloto(new Piloto(data[1]));
		  v.setId(Integer.parseInt(data[2]));
		  v.setTempo(LocalTime.parse("0:".concat(data[3]), DateTimeFormatter.ofPattern("H[H]:m:ss.SSS")));
		  v.setVelocidadeMedia(Double.parseDouble(data[4].replace(",", ".")));
		  
		  return v;
	};

}
