package com.corrida.model;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class VoltaUtilTest  {
	InputStream kartStream = null;
	
	@Before
	public void readResource() {
		this.kartStream = this.getClass().getClassLoader().getResourceAsStream("kart.txt");
	}


	/**
	 * Teste de leitura dos dados do arquivo kart.txt
	 */
	@Test
	public void testVoltaLoad() {
		
		List<Volta> voltas = VoltaUtil.load(this.kartStream);
		
		Volta v = voltas.get(0);
		
		assertEquals(LocalTime.parse("23:49:08.277"), v.getHora());
		assertEquals( "038", v.getPiloto().getCodigo());
		assertEquals("F.MASSA", v.getPiloto().getNome());
		assertEquals(Integer.valueOf(1), v.getId());
		assertEquals(LocalTime.parse("00:01:02.852"), v.getTempo());
		assertEquals(BigDecimal.valueOf(44.275), v.getVelocidadeMedia());
		
	}
}
