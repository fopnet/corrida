package com.corrida.model;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class CorridaTest {
	InputStream kartStream = null;

	@Before
	public void readResource() {
		this.kartStream = this.getClass().getClassLoader().getResourceAsStream("kart.txt");
	}

	/**
	 * Rigourous Test :-)
	 */
	@Test
	public void testInformacoesFMassa() {
	
		Corrida<Piloto> c = CorridaFactory.getCorridaKart(kartStream);

		List<Piloto> pilotos = c.calcularTempoDoVencedorEPosicaoChegada();

		Piloto fMassa = pilotos.get(0);
		Piloto vencedor = c.getVencedor();

		assertEquals(fMassa.getPosicaoChegada(), Integer.valueOf(1));
		assertEquals(fMassa.getNome(), "F.MASSA");
		assertEquals(fMassa.getCodigo(), "038");
		assertEquals(fMassa.getNumeroVoltasCompletas(), new Integer(4));
		assertEquals(fMassa.getTempoDecorrido(), Duration.parse("PT4M11.578S"));
		// Bonus
		assertEquals(fMassa.getTempoMelhorVolta(), Duration.parse("PT1M02.769S"));
		assertEquals(fMassa.calcularVelocidadeMedia() , BigDecimal.valueOf(44.245));
		assertEquals(fMassa.calcularTempoDecorridoDoVencedor(vencedor) , Duration.ZERO);

	}

	@Test
	public void testInformacoesKRAIKKONEN() {

		Corrida<Piloto> c = CorridaFactory.getCorridaKart(kartStream);

		List<Piloto> pilotos = c.calcularTempoDoVencedorEPosicaoChegada();

		Piloto haikkonen = pilotos.get(1);
		Piloto vencedor = c.getVencedor();

		assertEquals(haikkonen.getPosicaoChegada(), Integer.valueOf(2));
		assertEquals(haikkonen.getNome(), "K.RAIKKONEN");
		assertEquals(haikkonen.getCodigo(), "002");
		assertEquals(haikkonen.getNumeroVoltasCompletas(), new Integer(4));
		assertEquals(haikkonen.getTempoDecorrido(), Duration.parse("PT4M15.153S"));
		// Bonus
		assertEquals(haikkonen.getTempoMelhorVolta(), Duration.parse("PT1M03.076S"));
		assertEquals(haikkonen.calcularVelocidadeMedia() , BigDecimal.valueOf(43.627));
		assertEquals(haikkonen.calcularTempoDecorridoDoVencedor(vencedor) , Duration.parse("PT0M03.575S"));
	}

	@Test
	public void testInformacoesRBARRICHELLO() {

		Corrida<Piloto> c = CorridaFactory.getCorridaKart(kartStream);

		List<Piloto> pilotos = c.calcularTempoDoVencedorEPosicaoChegada();

		Piloto barrichello = pilotos.get(2);
		Piloto vencedor = c.getVencedor();

		assertEquals(barrichello.getPosicaoChegada(), Integer.valueOf(3));
		assertEquals(barrichello.getNome(), "R.BARRICHELLO");
		assertEquals(barrichello.getCodigo(), "033");
		assertEquals(barrichello.getNumeroVoltasCompletas(), new Integer(4));
		assertEquals(barrichello.getTempoDecorrido(), Duration.parse("PT4M16.080S"));
		// Bonus
		assertEquals(barrichello.getTempoMelhorVolta(), Duration.parse("PT1M03.716S"));
		assertEquals(barrichello.calcularVelocidadeMedia() , BigDecimal.valueOf(43.468));
		assertEquals(barrichello.calcularTempoDecorridoDoVencedor(vencedor) , Duration.parse("PT0M04.502S"));
		
	}

	@Test
	public void testInformacoesMWEBBER() {

		Corrida<Piloto> c = CorridaFactory.getCorridaKart(kartStream);

		List<Piloto> pilotos = c.calcularTempoDoVencedorEPosicaoChegada();

		Piloto mWebber = pilotos.get(3);
		Piloto vencedor = c.getVencedor();

		assertEquals(mWebber.getPosicaoChegada(), Integer.valueOf(4));
		assertEquals(mWebber.getNome(), "M.WEBBER");
		assertEquals(mWebber.getCodigo(), "023");
		assertEquals(mWebber.getNumeroVoltasCompletas(), new Integer(4));
		assertEquals(mWebber.getTempoDecorrido(), Duration.parse("PT4M17.722S"));
		// Bonus
		assertEquals(mWebber.getTempoMelhorVolta(), Duration.parse("PT1M04.216S"));
		assertEquals(mWebber.calcularVelocidadeMedia() , BigDecimal.valueOf(43.191));
		assertEquals(mWebber.calcularTempoDecorridoDoVencedor(vencedor) , Duration.parse("PT0M06.144S"));

	}

	@Test
	public void testInformacoesFALONSO() {

		Corrida<Piloto> c = CorridaFactory.getCorridaKart(kartStream);

		List<Piloto> pilotos = c.calcularTempoDoVencedorEPosicaoChegada();

		Piloto fAlonso = pilotos.get(4);
		Piloto vencedor = c.getVencedor();

		assertEquals(fAlonso.getPosicaoChegada(), Integer.valueOf(5));
		assertEquals(fAlonso.getNome(), "F.ALONSO");
		assertEquals(fAlonso.getCodigo(), "015");
		assertEquals(fAlonso.getNumeroVoltasCompletas(), new Integer(4));
		assertEquals(fAlonso.getTempoDecorrido(), Duration.parse("PT4M54.221S"));
		// Bonus
		assertEquals(fAlonso.getTempoMelhorVolta(), Duration.parse("PT1M07.011S"));
		assertEquals(fAlonso.calcularVelocidadeMedia() , BigDecimal.valueOf(38.066));
		assertEquals(fAlonso.calcularTempoDecorridoDoVencedor(vencedor) , Duration.parse("PT0M42.643S"));
		
	}

	@Test
	public void testInformacoesSVETTEL() {

		Corrida<Piloto> c = CorridaFactory.getCorridaKart(kartStream);

		List<Piloto> pilotos = c.calcularTempoDoVencedorEPosicaoChegada();

		Piloto svettel = pilotos.get(5);
		Piloto vencedor = c.getVencedor();

		assertEquals(svettel.getPosicaoChegada(), Integer.valueOf(6));
		assertEquals(svettel.getNome(), "S.VETTEL");
		assertEquals(svettel.getCodigo(), "011");
		assertEquals(svettel.getNumeroVoltasCompletas(), new Integer(3));
		assertEquals(svettel.getTempoDecorrido(), Duration.parse("PT6M27.276S"));
		// Bonus
		assertEquals(svettel.getTempoMelhorVolta(), Duration.parse("PT1M18.097S"));
		assertEquals(svettel.calcularVelocidadeMedia() , BigDecimal.valueOf(25.745));
		assertEquals(svettel.calcularTempoDecorridoDoVencedor(vencedor) , Duration.parse("PT2M15.698S"));
	}
	
	@Test
	public void testMelhorVoltaCorrida() {
		
		Corrida<Piloto> c = CorridaFactory.getCorridaKart(kartStream);
		
		// Bonus
		assertEquals(c.getTempoMelhorVolta(), Duration.parse("PT1M02.769S"));
		
	}

	@Test
	public void testDurationMinus() {

		Duration vencedor = Duration.parse("PT4M11.558S");
		Duration d2 = Duration.parse("PT4M15.153S");
		Duration d3 = Duration.parse("PT4M16.080S");
		
		// Bonus
		assertEquals(d2.minus(vencedor), Duration.parse("PT0M03.595S"));
		assertEquals(d3.minus(vencedor), Duration.parse("PT0M04.522S"));

	}

}
